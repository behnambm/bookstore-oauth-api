package db

import (
	"github.com/gocql/gocql"
	"gitlab.com/behnambm/bookstore-oauth-api/src/clients/cassandra"
	"gitlab.com/behnambm/bookstore-oauth-api/src/domain/access_token"
	"gitlab.com/behnambm/bookstore-oauth-api/src/utils/errors"
)

const (
	queryGetAccessToken    = "SELECT access_token, user_id, client_id, expires FROM access_tokens WHERE access_token=?;"
	queryCreateAccessToken = "INSERT INTO access_tokens (access_token, user_id, client_id, expires) VALUES(?, ?, ?, ?);"
	queryUpdateExpires     = "UPDATE access_tokens SET expires=? WHERE access_token=?;"
)

type DBRepository interface {
	GetById(string) (*access_token.AccessToken, *errors.RestErr)
	Create(access_token.AccessToken) *errors.RestErr
	UpdateExpirationTime(access_token.AccessToken) *errors.RestErr
}

type dbRepository struct {
}

func NewRepository() DBRepository {
	return &dbRepository{}
}

func (r *dbRepository) GetById(id string) (*access_token.AccessToken, *errors.RestErr) {
	var result access_token.AccessToken
	if qryErr := cassandra.GetSession().Query(queryGetAccessToken, id).Scan(
		&result.AccessToken,
		&result.UserId,
		&result.ClientId,
		&result.Expires,
	); qryErr != nil {
		if qryErr == gocql.ErrNotFound {
			return nil, errors.NewNotFoundError("access_token not found")
		}
		return nil, errors.NewInternalServerError(qryErr.Error())
	}

	// todo: get access token from Cassandra db.
	return nil, errors.NewInternalServerError("database connection not implemented")
}

func (r *dbRepository) Create(token access_token.AccessToken) *errors.RestErr {
	if insrtErr := cassandra.GetSession().Query(
		queryCreateAccessToken,
		token.AccessToken,
		token.UserId,
		token.ClientId,
		token.Expires,
	).Exec(); insrtErr != nil {
		return errors.NewInternalServerError(insrtErr.Error())
	}
	return nil
}

func (r *dbRepository) UpdateExpirationTime(token access_token.AccessToken) *errors.RestErr {
	if updtErr := cassandra.GetSession().Query(
		queryUpdateExpires,
		token.Expires,
		token.AccessToken,
	).Exec(); updtErr != nil {
		return errors.NewInternalServerError(updtErr.Error())
	}
	return nil
}
