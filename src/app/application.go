package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/behnambm/bookstore-oauth-api/src/clients/cassandra"
	"gitlab.com/behnambm/bookstore-oauth-api/src/domain/access_token"
	"gitlab.com/behnambm/bookstore-oauth-api/src/http"
	"gitlab.com/behnambm/bookstore-oauth-api/src/repository/db"
)

var (
	router = gin.Default()
)

func StartApplication() {
	// test connection to DB
	session, dbErr := cassandra.GetSession()
	if dbErr != nil {
		panic(dbErr)
	}
	session.Close()

	atHandler := http.NewHandler(access_token.NewService(db.NewRepository()))

	router.GET("/oauth/access_token/:access_token_id", atHandler.GetById)
	router.POST("/oauth/access_token/", atHandler.Create)

	router.Run(":8080")
}
