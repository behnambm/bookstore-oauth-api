package main

import "gitlab.com/behnambm/bookstore-oauth-api/src/app"

func main() {
	app.StartApplication()
}
